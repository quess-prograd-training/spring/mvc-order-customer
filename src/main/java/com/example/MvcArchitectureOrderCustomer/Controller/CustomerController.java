package com.example.MvcArchitectureOrderCustomer.Controller;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Customer;
import com.example.MvcArchitectureOrderCustomer.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerServiceObject;

    @PostMapping("/adding")
    public String addUser(@RequestBody Customer customerObject ){
        String str = String.valueOf(customerServiceObject.addCustomers(customerObject));
        return str;
    }

    @GetMapping("/getCustomers")
    public List<Customer> display(@PathVariable int customerId){
        return customerServiceObject.getCustomer(customerId);
    }
}
