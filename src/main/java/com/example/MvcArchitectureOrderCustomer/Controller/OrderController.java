package com.example.MvcArchitectureOrderCustomer.Controller;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Order;
import com.example.MvcArchitectureOrderCustomer.Service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {
    @Autowired
    OrderService orderServiceObject;

    @PostMapping("/orders")
    public String addOrder(@RequestBody Order orderObject ){
        String str = String.valueOf(orderServiceObject.addOrders(orderObject));
        return str;
    }

    @GetMapping("/getOrders")
    public List<Order> display(){
        return orderServiceObject.getOrders();
    }
}
