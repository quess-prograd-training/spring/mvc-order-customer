package com.example.MvcArchitectureOrderCustomer.Repository;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order,InternalError> {

    Optional<Object> findByOrderId(int i);
}
