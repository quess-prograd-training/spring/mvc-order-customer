package com.example.MvcArchitectureOrderCustomer.Repository;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
}
