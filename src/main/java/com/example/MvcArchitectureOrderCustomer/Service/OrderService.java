package com.example.MvcArchitectureOrderCustomer.Service;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Order;
import com.example.MvcArchitectureOrderCustomer.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepositoryObject;
    public Order addOrders(Order orderObject) {
        return orderRepositoryObject.save(orderObject);
    }

    public List<Order> getOrders() {
        return orderRepositoryObject.findAll();
    }
}
