package com.example.MvcArchitectureOrderCustomer.Service;

import com.example.MvcArchitectureOrderCustomer.ModelEntity.Customer;
import com.example.MvcArchitectureOrderCustomer.ModelEntity.Order;
import com.example.MvcArchitectureOrderCustomer.Repository.CustomerRepository;
import com.example.MvcArchitectureOrderCustomer.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepositoryObject;
    @Autowired
    OrderRepository orderRepositoryObject;
    public Customer addCustomers(Customer customerObject) {
        List<Customer> customerList=customerRepositoryObject.findAll();
        if (!customerList.isEmpty()){//if user available then make role as user
            Order orders= (Order) orderRepositoryObject.findByOrderId(1).orElseThrow();
            Set<Order> orderObject=new HashSet<>();
            orderObject.add(orders);

            customerObject.setOrders(orderObject);
            return customerRepositoryObject.save(customerObject);
        }

        return customerRepositoryObject.save(customerObject);
    }

    public List<Customer> getCustomer(int customerId) {
        return customerRepositoryObject.findAll();
    }
}
