package com.example.MvcArchitectureOrderCustomer.ModelEntity;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table
public class Customer {
    @Id
    private int customerId;
    private String customerAddress;
    private int customerMobileNo;
    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "customer_orders",
            joinColumns = { @JoinColumn(name = "customerId") },
            inverseJoinColumns = { @JoinColumn(name = "orderId") })
    private Set<Order> orders = new HashSet<>();

    public Customer() {
    }

    public Customer(int customerId, String customerAddress, int customerMobileNo) {
        this.customerId = customerId;
        this.customerAddress = customerAddress;
        this.customerMobileNo = customerMobileNo;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public int getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(int customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public void setOrders(Set<Order> orderObject) {
    }
}
