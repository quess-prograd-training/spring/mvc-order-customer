package com.example.MvcArchitectureOrderCustomer.ModelEntity;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table
public class Order {
    @Id
    private int orderId;
    private String orderDescription;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },mappedBy = "orders")
    private Set<Customer> customers = new HashSet<>();

    public Order() {
    }

    public Order(int orderId, String orderDescription) {
        this.orderId = orderId;
        this.orderDescription = orderDescription;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }
}
