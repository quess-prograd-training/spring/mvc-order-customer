package com.example.MvcArchitectureOrderCustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MvcArchitectureOrderCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcArchitectureOrderCustomerApplication.class, args);
	}

}
